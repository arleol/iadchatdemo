******Présentation de IAD Chat******
mercredi, 03. octobre 2018 07:05 


IADChat est un chat en MVC destiné à évaluer les compétences de son auteur.

Son architeture est la suivante
		index.html est une page html / jquery qui affiche l'interface du chat et entretient la communication avec les serveur PHP
		
		Cette page a trois états:
			- Etat de Login avec un Nom et Mot de passe
			- Etat Chat avec les messages recpetion et enoi de message
			- Etat de creation de compte 
			
		Selon les options des pages, un requete ajax est faite au serveur sur le process.php. Ce process lance une instance du contrôleur qui assurera la logique du Chat. Le Chat gère trois classes rémanentes:
		-les USERS (Avec Password en clair pour la démonstration)
		-les sessions(Avec date de création et de dernière utilisation de la session et User propriétaire de la SESSION.)
		-les  messages échangées, le User qui l 'a envoyé, et la date d 'envoi.
		
On retrouvera cette architecture dans l'arborescence du projet:
		- sous web et les sous répertoires, le fichier index.html et le fichier process.php,  et les fichiers css et js attenant.
		- sous src dans Model les trois classes correspondantes aux objets décrits ci dessus.
		- sous src/Model/Mysqllayer une classe IADMysqlLayer qui permet de mapper les objets sur du relationnel (le script de création de la table est fourni dans ce répertoire)
		- sous src/Controller on trouvera la classe du controlleur instanciée dans process.php
		
***Installation***
		
Sur la machine hôte, sous le repertoire courant du serveur (ici apache)
/var/www passer la commande:

	git clone git@gitlab.com:arleol/iadchatdemo.git

dans le répertoire IADChatDemo/src/Parameters renommer le fichier dbconnect.exemple en dbconnect.ini.
Si vous souhaitez changer le nom et le mot de passe d'accès au serveur mysql, ainsi que l'adresse du serveur editer ce fichier. Mais avant de lancer le script d'installation du schéma sql sur le serveur MariaDb, il faudra editer ce script pour que les user et mot de passe correspondent.
Par défaut, le serveur DB et sur la même machine que le serveur apache (**localhost**) la base s'appelle **IDAchat**, le compte mysql créé pour acceder à cette base est **idaUser **& **idaPassword**

Le script de création de la base de donnée se trouve sous le répertoire Documentation dans IDAchat.sql

**Fonctionnement**
Allez sur le site
Lancer l'application ex: [Un site de démonstartion](http://iadchat.odass.org) 
	Entrez nom et mot de passe:
	- Si le nom existe et que le mot de passe est ok vous entrez dans le chat
	- Si le nom existe et que le mot de passe est mauvais, vous restez sur la page d'accueil
	- le nom n'existe pas une bouton "Céer le login apparait" si vous cliquez sur ce bouton, le compte est créé et vous passez sur la page chat
	- la page chat, vous pouvez rentrer un message. Lorsque vous validez par "return" ce message est envoyé à tout le monde. 
	Une liste des personnes connectés est  disponibles sur le droite du message.
	Un bouton logout permet de sortir de la page chat. 
	Cette page est rafraichie toute les cinq secondes.
	Si vous fermez la page sans logout, un time out serveur fermera votre session au bout de dix minutes.
	
	**NB:** 
	Les mot de passe sont en clair sur la base de donnée, pour des raisons de démonstration. 
	Les logins et mot de passe ne sont pas vérifiées à la saisie, mais on attend une chaine de caractere sans espace.
	Le graphisme est résumé à sa plus simple expression, la volonté de l auteur étant plutôt de démontrer sa compréhension et sa capacité à mettre en place un MVC objet maintenable et documenté. 
	Enfin, la console javascript permet de suivre les différents échanges avec le serveur.
	
	Bonne évaluation. 
	Cordialement. 
	
				Olivier
	

