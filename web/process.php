<?php
    namespace IADchat\web;
    require_once(__DIR__."/../src/Model/Mysqlayer/IADMysqlLayer.php");
    require_once(__DIR__."/../src/Controller/IADChatController.php");
    require_once(__DIR__."/../src/Model/IADChatExchange.php");
    require_once(__DIR__."/../src/Model/IADUser.php");
    require_once(__DIR__."/../src/Model/IADSession.php");

    use IADchat\src\Model\Mysqlayer\IADMysqlLayer;
    use IADchat\src\Model\IADUser;
    use IADchat\src\Model\IADSession;
    use IADchat\src\Model\IADChatExchange;
    use IADchat\src\Controller\IADChatController;

    $controleur = new IADChatController();
    $reponse = $controleur->handleRequest($_POST);

    echo json_encode($reponse);

?>