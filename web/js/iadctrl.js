/*
Created by: Kenrick Beckett
Name: Chat Engine
*/

var instanse = false;
var state;
var mes;
var file;
var lstatus;
var X;

function Chat() {
    this.update = updateChat;
    this.send = sendChat;
    this.checkLogin = checkLogin;
    this.createLogin = createLogin;
    this.getState = getStateOfChat;
    this.logout = logout;
    this.updateView = updateView;
}

function updateView(data) {
    lstatus = data.AUTH;
    console.log("updateView", data.MESSAGE, data);
    switch (lstatus) {
        case "CHAT":
            console.log("checkLogin", "CHAT");
            $("#page-login").hide();
            $("#page-login-create").hide();
            $("#page-wrap").show();
            var x = setTimeout(chat.update, 5000);
            break;
        case "CREAT":
            console.log("checkLogin", "CREAT");
            $("#page-login").show();
            $("#page-login-create").show();
            $("#page-wrap").hide();
            clearTimeout(X);
            break;
        case "LOGIN":
            console.log("checkLogin", "LOGIN");
            $("#page-login").show();
            $("#page-login-create").hide();
            $("#page-wrap").hide();
            clearTimeout(X);
            break;
        }
        $('#chat-area').html("");
        if (data.LMSG) {
            for (var i = 0; i < data.LMSG.length; i++) {
                $('#chat-area').append($("<p><span>" + data.LMSG[i].date + " " + data.LMSG[i].sender + "</span>" + data.LMSG[i].msg + "</p>"));
            }
        }
        //document.getElementById('chat-area').scroll= document.getElementById('chat-area').scrollHeight;

        $('#logged-area').html("<ul></ul>");
        if (data.LUSERS) {
            for (var i = 0; i < data.LUSERS.length; i++) {
                $('#logged-area').append($("<li>" + data.LUSERS[i].date + "<br>" + data.LUSERS[i].user + "</li>"));
            }
        }
        // document.getElementById('logged-area').scrollBottom = document.getElementById('chat-area').scrollHeight;


        instanse = false;
    }

//gets the state of the chat
function getStateOfChat() {
    if (!instanse) {
        instanse = true;
        $.ajax({
            type: "POST",
            url: "process.php",
            data: {
                'function': 'getState',
                'file': file
            },
            dataType: "json",

            success: function (data) {
                instanse = false;
                updateView(data);
            },
        });
    }
}

function createLogin(login, password) {
    if (!instanse) {
        instanse = true;
        $.ajax({
            type: "POST",
            url: "process.php",
            data: {
                'function': 'createLogin',
                'login': login,
                'password': password
            },
            dataType: "json",
            error: function (jqxhr, status, error) {
                instanse = false;
                console.log("createLogin", "echec", status);
            },
            success: function (data) {
                updateView(data);
                instanse = false;
            },
        });
    }
}

function checkLogin(login, password) {

        $.ajax({
            type: "POST",
            url: "process.php",
            data: {
                'function': 'checkLogin',
                'login': login,
                'password': password
            },
            dataType: "json",
            error: function (jqxhr, status, error) {
                instanse = false;
                console.log("checkLogin", "echec", status);
            },
            success: function (data) {
                instanse = false;
                updateView(data);
            },
        });
}

//Updates the chat
function updateChat() {
    if (!instanse) {
        instanse = true;
        $.ajax({
            type: "POST",
            url: "process.php",
            data: {
                'function': 'update',
                'state': state,
                'file': file
            },
            dataType: "json",
            success: function (data) {
                instanse = false;
                updateView(data);
            },
        });
    }
}

//send the message
function sendChat(message) {
    console.log("send", message);
    $.ajax({
        type: "POST",
        url: "process.php",
        data: {
            'function': 'send',
            'message': message
        },
        dataType: "json",
        success: function (data) {

            updateView(data);
        },
    });
}

//send the message
function logout() {
    console.log("logout");
    $.ajax({
        type: "POST",
        url: "process.php",
        data: {
            'function': 'logout'
        },
        dataType: "json",
        success: function (data) {
            updateView(data);
        },
    });
}

