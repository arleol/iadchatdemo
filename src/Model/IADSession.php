<?php
use IADchat\src\Model\IADUser;
use IADchat\src\Model\Mysqlayer\IADMysqlLayer;
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 30/09/18
 * Time: 12:29
 * Cette classe gère les sessions. Si elle existe, la session a été authentifiée. Elle permet de connaitre le delta entre
 * sa dernière mise à jour et l'instant, donc de déterminer le délai entre deux interrogations
 * La détermination
 *
 *
 */

class IADSession
{
    /**
     * @return int
     */
    public function getIdsql(): int
    {
        return $this->idsql;
    }

    /**
     * @param int $idsql
     */
    public function setIdsql(int $idsql): void
    {
        $this->idsql = $idsql;
    }

    /**
     * @return mixed
     */
    public static function getNamesql()
    {
        return self::$namesql;
    }



    /**
     * @return DateTime
     */
    public function getCreateddate(): DateTime
    {
        return $this->createddate;
    }

    /**
     * @param DateTime $createddate
     */
    public function setCreateddate(DateTime $createddate): void
    {
        $this->createddate = $createddate;
    }

    /**
     * @return DateTime
     */
    public function getLastupdate(): DateTime
    {
        return $this->lastupdate;
    }

    /**
     * @param DateTime $lastupdate
     */
    public function setLastupdate(DateTime $lastupdate): void
    {
        $this->lastupdate = $lastupdate;
    }

    /**
     * @return \IADUser
     */
    public function getUser(): \IADUser
    {
        return $this->user;
    }

    /**
     * @param \IADUser $user
     */
    public function setUser(\IADUser $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getSessionid(): string
    {
        return $this->sessionid;
    }

    /**
     * @param string $sessionid
     */
    public function setSessionid(string $sessionid): void
    {
        $this->sessionid = $sessionid;
    }
    /**
     * identifiant mysql dans la
     *
     * @var integer
     *
     */
    protected $idsql;

    /**
     * Nom de la table pour péréniser l'objet (Pas trop le temps de réécrire doctrine)
     */
    protected static $namesql = "IADSession";


    /**
     * Date de création de la session
     * @var \DateTime
     */
    private $createddate;


    /**
     * Date de dernière mise à jour de la session
     * @var \DateTime
     */
    private $lastupdate;

    /**
     * Lien vers le User prpriétaire de la session lors de l'authentification
     *
     * @var \IADUser
     *
     */
    private $user;

    /**
     * Identifiant de la session au sens PHP (php session id)
     * @var string
     */
    private $sessionid;
}