<?php
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 30/09/18
 * Time: 14:35
 */
namespace IADchat\src\Model\Mysqlayer;
use IADchat\src\Model\IADUser;
use IADchat\src\Model\IADChatExchange;
use IADchat\src\Model\IADSession;
class IADMysqlLayer
{
    private $connection;
    private $paramfile = __DIR__."/../../Parameters/dbconnect.ini";
    /**
     * Cette classe établit la connection avec la BD Mysql selon les paramêtres présent dans le fichier $paramfile
     *
     * @return PDO
     */
    public function setConnection()
    {
        $config = parse_ini_file($this->paramfile);
        // var_dump($config);
        $this->connection = new \PDO('mysql:host='.$config['servername'].";dbname=".$config['dbname'],
            $config['username'],
            $config['password'],
            array( \PDO::ATTR_PERSISTENT => true ));
    }
    public function testConnection(){
        $totalExchanges = $this->connection->query('SELECT count(*) as totalExchanges FROM IADChatExchange;')->fetchColumn();
        return $totalExchanges;
    }

    /**
     * On vérifie si le user existe.
     *  Si oui on renvoie son ID
     *  Si non on le crée.
     *
     * @param IADUser $user
     * @return int
     */
    public function createUser($login, $password){
        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'INSERT INTO IDAUser (username, password) VALUES (:user,:pass )';
        $stmt = $this->connection->prepare($sql);


        $data = ['user'=>$login,'pass'=>$password];
        try {
            $stmt->execute($data);
        }
        catch  (\PDOException $e){
            throw $e;
        }
        $id = $this->connection->lastInsertId();
        if ($id != 0){
            $user = new \IADUser();
            $user->setUsername($login);
            $user->setPassword($password);
            $user->setIdsql($id);
        }
        else
            $user=null;
        return $user;
    }
    /**
     * On vérifie si le user existe.
     *  Si oui on renvoie son ID
     *  Si non on le crée.
     *
     * @param string $sid
     * @return \IADSession $session;
     */
    public function CreateOrUpdateSession($sid, \IADUser $user){
        $update=false;
        $retid=null;
        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'SELECT id FROM IADSession WHERE sessionid = :ses';
        $stmt = $this->connection->prepare($sql);

        try {
            $rc = $stmt->execute(['ses'=> $sid]);
            // var_dump($rc);
        }
        catch  (\PDOException $e){
            throw $e;
        }
        if (isset($rc)) {
            $wses = $stmt->fetch();
            if ($wses){
                $update=true;
                $retid =  $wses['id'];
            }
        }
        if (!$update) {
            $sql = 'INSERT INTO IADSession (sessionid, createddate, updatetddate, IADUserid) VALUES (:sid,:crd, :upd, :uid )';
            $stmt = $this->connection->prepare($sql);
            $t = new \DateTime();
            $ts = $t->format('Y-m-d H:i:s');
            $data = ['sid' => $sid, 'crd' => $ts, 'upd' => $ts, 'uid' => $user->getIdsql()];
            try {
                $stmt->execute($data);
            } catch (\PDOException $e) {
                throw $e;
            }
            $id = $this->connection->lastInsertId();
            return $id;
        }
        else{
            $sql = 'UPDATE IADSession SET updatetddate = :upd WHERE id = :id';
            $stmt = $this->connection->prepare($sql);
            $t = new \DateTime();
            $ts = $t->format('Y-m-d H:i:s');
            $data = ['upd' => $ts, 'id' => $retid];
            try {
                $stmt->execute($data);
            } catch (\PDOException $e) {
                throw $e;
            }
            return $retid;
        }
    }
    /**
     * Ajoute un message à la base message
     * @param \IADUser $user
     * @param string $message
     * @return \IADChat
     */
    public function addMessage(\IADUser $user, $message){
        if (!$this->connection){
            $this->setConnection();
        }
        $msg= new IADChatExchange();
        $msg->setSender($user);
        $msg->setDate(new \DateTime());
        $msg->setExchangetext($message);
        $sql = 'INSERT INTO IADChatExchange (date, sender, exghangetext) VALUES (:date, :uid, :msg )';
        $stmt = $this->connection->prepare($sql);
        $t = new \DateTime();
        $ts = $t->format('Y-m-d H:i:s');
        $qmsg=$message;
        $data = ['date' => $ts, 'uid' => $user->getIdsql(), 'msg' => $qmsg];
        try {
            $stmt->execute($data);
        } catch (\PDOException $e) {
            throw $e;
        }
        $id = $this->connection->lastInsertId();
        $ec=$this->connection->errorInfo();
        $msg->setIdsql($id);
        return $msg;
    }
    /**
     * Retourne la totalités des messages la dat et les senders
     * NB Dans un mond parfait, on ne retournrait pas défaut que les 24 dernières heures
     * en differentiel d'un paramêtre date venu du client
     * * @return array
     */
    public function getMessages(){
        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'SELECT * FROM IADChatExchange msg, IDAUser as usr WHERE msg.sender = usr.id ORDER BY msg.date DESC;';
        $stmt = $this->connection->prepare($sql);
        try {
            $rc = $stmt->execute();
        } catch (\PDOException $e) {
            throw $e;
        }
        $listmsg = [];
        if (isset($rc)) {
            $wmsg = $stmt->fetchall();
            foreach ($wmsg as $msg){
                $listmsg[]= ['msg'=>$msg['exghangetext'], 'date'=>$msg['date'], 'sender'=>$msg['username']];
            }
        }
        return $listmsg;
    }
    /**
     * Retourne touts les user loggé dont l'activité est plus récente de 10 minutes
     *
     * @return array
     */
    public function getLoggeduser(){
        $sql = ' SELECT  MAX(updatetddate), t2.username FROM IADSession AS t1 INNER JOIN IDAUser AS t2 ON t1.IADUserId = t2.id  
          WHERE timestampdiff(SECOND, updatetddate, now() ) < 600 GROUP BY t2.id  ORDER BY t2.username ASC ;';
        $stmt = $this->connection->prepare($sql);
        try {
            $rc = $stmt->execute();
        } catch (\PDOException $e) {
            throw $e;
        }
        $listmsg = [];
        if (isset($rc)) {
            $wmsg = $stmt->fetchall();
            foreach ($wmsg as $msg){
                $listmsg[]= ['date'=>$msg['MAX(updatetddate)'], 'user'=>$msg['username']];
            }
        }
        return $listmsg;
    }
    /**
     * Mise à jour du time stamp last update
     * @param string $sid
     *
     */
    public function UpdateSession($sid){
        if (!$this->connection){
            $this->setConnection();
        }

        $sql = 'UPDATE IADSession SET updatetddate = :upd WHERE sessionid = :sid';
        $stmt = $this->connection->prepare($sql);
        $t = new \DateTime();
        $ts = $t->format('Y-m-d H:i:s');
        $data = ['upd' => $ts, 'sid' => $sid];
        try {
             $stmt->execute($data);
        } catch (\PDOException $e) {
            throw $e;
        }
    }
    /**
     * Retourne la session ou null
     *
     * @param string $sid
     * @return \IADSession $session;
     */
    public function GetSession($sid){

        if (!$this->connection){
            $this->setConnection();
        }

        $sql = 'SELECT * FROM IDAchat.IADSession as sess, IDAchat.IDAUser as user WHERE sess.sessionid=:sid AND sess.IADUserId = user.id';
        $stmt = $this->connection->prepare($sql);
        $data = ['sid' => $sid];
        try {
            $rc = $stmt->execute($data);
        }
        catch  (\PDOException $e){
            throw $e;
        }
        if (isset($rc)) {
            $wsession = $stmt->fetch();
            if ($wsession){
                $user=new \IADUser();
                $user->setIdsql($wsession['id']);
                $user->setUsername($wsession['username']);
                $user->setPassword($wsession['password']);
                $session = new \IADSession();
                $session->setIdsql($wsession[0]);
                $session->setCreateddate(new \DateTime($wsession['createddate']));
                $session->setLastupdate(new \DateTime($wsession['updatetddate']));
                $session->setUser($user);
                return $session;
            }
            else
                return $wsession;
        }

    }


    /**
     * Détruire la session
     *
     * @param string $sid
     * @return \IADSession $session;
     */
    public function RemoveSession($sid){

        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'DELETE FROM IDAchat.IADSession WHERE sessionid=:sid';
        $stmt = $this->connection->prepare($sql);
        $data = ['sid' => $sid];
        try {
            $rc = $stmt->execute($data);
        }
        catch  (\PDOException $e){
            throw $e;
        }
    }

    /**
     * @param string $ln
     * @return IADUser $user or null
     */
    public function searchUserByLoginName($ln){
        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'SELECT * FROM IDAUser WHERE username = :user';
        $stmt = $this->connection->prepare($sql);

        try {
            $rc = $stmt->execute(['user'=> $ln]);
        }
        catch  (\PDOException $e){
            throw $e;
        }
        if (isset($rc)) {
            $wuser = $stmt->fetch();
            if ($wuser){
                $user = new \IADUser();
                $user->setIdsql($wuser['id']);
                $user->setUsername($wuser['username']);
                $user->setPassword($wuser['password']);
                return $user;
            }
            else
                return null;
        }

    }

    /**
     * @param string $id
     * @return IADUser $user or null
     */
    public function searchUserById($id){
        if (!$this->connection){
            $this->setConnection();
        }
        $sql = 'SELECT * FROM IDAUser WHERE id = :id';
        $stmt = $this->connection->prepare($sql);

        try {
            $rc = $stmt->execute(['id'=> $id]);
        }
        catch  (\PDOException $e){
            throw $e;
        }
        if (isset($rc)) {
            $wuser = $stmt->fetch();
            if ($wuser){
                $user = new \IADUser();
                $user->setIdsql($wuser['id']);
                $user->setUsername($wuser['username']);
                $user->setPassword($wuser['password']);
                return $user;
            }
            else
                return null;
        }

    }


}