<?php
namespace IADchat\src\Model;

use IADUser;
use IADMysqlLayer;
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 30/09/18
 * Time: 12:31
 *
 * Cette classe représente un échange dans le CHAT, à savoir la chaine de caractère envoyée lors d'un return de du user sur la fenêtre de chat
 */

class IADChatExchange
{

    /**
     * identifiant mysql dans la
     * @var integer
     *
     */
    protected $idsql;

    /**
     * Nom de la table pour péréniser l'objet (Pas trop le temps de réécrire doctrine)
     */
    protected static $namesql = "IADChatEcxhange";

    /**
     * Date de l'échange.
     * @var \DateTime
     */
    private $date;



    /**
     * Identité du user qui a ecrit ce échange
     * @var \IADUser
     */
    private $sender;

    /**
     * @var string
     *
     */
    private $exchangetext;



    /**
     * @return int
     */
    public function getIdsql(): int
    {
        return $this->idsql;
    }

    /**
     * @param int $idsql
     */
    public function setIdsql(int $idsql): void
    {
        $this->idsql = $idsql;
    }


    /**
     * Set date
     * Sans argument, prend la date et l'heure courante
     *
     * @var \DateTime
     */
    public function setDate(\DateTime $wdate = null)
    {
        if ($wdate === null)
            $this->date = new DateTime();
        else
            $this->date = $wdate;
    }

    /**
     * @return \IADUser
     */
    public function getSender(): \IADUser
    {
        return $this->sender;
    }

    /**
     * @param \IADUser $sender
     */
    public function setSender(\IADUser $sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getExchangetext(): string
    {
        return $this->exchangetext;
    }

    /**
     * @param string $exchangetext
     */
    public function setExchangetext(string $exchangetext): void
    {
        $this->exchangetext = $exchangetext;
    }
}