<?php
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 30/09/18
 * Time: 12:29
 * Cette classe gère les comptes utilisateur. Elle est vraiment basique avec nom user et mot de passe en clair, sans
 * déactivation possible ni cryptage dans le cadre de cet exercice.
 *
 *  Le user sert à vérifier le mot de passe et à attacher les sessions à un user
 */
class IADUser
{
    /**
     * identifiant mysql dans la
     *
     *
     * @var integer
     *
     */
    protected $idsql;

    /**
     * Nom de la table pour péréniser l'objet (Pas trop le temps de réécrire doctrine)
     */
    protected static $namesql = "IADUser";

    /**
     * User login
     *
     * @var string
     *
     *
     */
    private $username;

    /**
     * User Password (En clair pour l'exercice. Dans le monde "réel" de l'internet, évidemment crypté avec un
     * mot cle aléatoire (ou salt) et des méthodes de réinitialisation par jeton via mail internet ou sms
     */

    private $password;

    /**
     * @return int
     */
    public function getIdsql(): int
    {
        return $this->idsql;
    }

    /**
     * @param int $idsql
     */
    public function setIdsql(int $idsql): void
    {
        $this->idsql = $idsql;
    }

    /**
     * @return mixed
     */
    public static function getNamesql()
    {
        return self::$namesql;
    }

     /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }
}