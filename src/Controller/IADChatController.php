<?php
namespace IADchat\src\Controller;
/**
 * Created by PhpStorm.
 * User: olivier
 * Date: 30/09/18
 * Time: 12:26
 * Ce controlleur vérifie la viabilité l' Accès aux objets et data sousjascente - ici MySql
 * Puis il regarde l'état de l'actuelle Session
 *  - est elle enregistrée
 *  - est en time out
 * Si ces conditions sont remplis, il mets à jour le DateTime de la session puis il execute la demande transmise
 * Sinon il renvoie à la page de login
 */

use IADSession;
use IADUser;
use IADchat\src\Model\IADChatExchange;
use IADchat\src\Model\Mysqlayer\IADMysqlLayer;

class IADChatController
{
    private $post;
    private $user;
    private $session;

    /**
     * @param $post array
     * @return $response array
     */
    public function handleRequest($post)
    {
        if (!isset($post['function'])) {
            throw(new Exception("Fonction absente"));
        }
        $this->post = $post;
        if ($this->post['function'] == "checkLogin") {
            return $this->checkLogin();
        } elseif ($this->post['function'] == "logout") {
            return $this->logout();
        } elseif ($this->post['function'] == "createLogin") {
            return $this->createLogin();
        } elseif ($this->post['function'] == "send") {
            return $this->send();
        } elseif ($this->post['function'] == "update") {
            return $this->update();
        } else
            return ["AUTH" => "LOGIN", "MESSAGE" => "ERREUR DE ROUTAGE", "LMSG" => [], "LUSERS" =>[]];
    }

    /**
     * Vérifie le login et renvoie trois cas
     *  login inexistant -> propose la création
     *  login faux -> retourne au login
     *  login ok -> renvoie au panneau de chat avec liste des messages et liste des connectés
     * @return $response array
     */
    public function checkLogin()
    {
        $wlogin = $this->post['login'];
        $wpassword = $this->post['password'];
        $reponse = [];
        $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
        $user = $sqllayer->searchUserByLoginName($wlogin);
        if ($user == null) {
            $reponse = ["AUTH" => "CREAT", "MESSAGE" => "LOGIN INEXISTANT, CREATION AUTORISE", "LMSG" => [], "LUSERS" =>[]];
            return $reponse;
        } elseif ($user->getPassword() == $wpassword) {
            session_start();
            $sid = session_id();
            $sqllayer->CreateOrUpdateSession($sid, $user);
            return $this->returnChatdata();
        } elseif ($user->getPassword() != $wpassword) {
            $reponse = ["AUTH" => "LOGIN", "MESSAGE" => "LOGIN EXISTANT MAUVAIS MOT DE PASSE", "LMSG" => [], "LUSERS" =>[]];
            return $reponse;
        }
    }

    /**
     * Vérifie la validité de la session (session connue et timeout)
     *  si faux  -> retourne au login
     *  si vrai -> enregistre le message et branche du update chat
     * @return $response array
     */
    public function send()
    {
        if ($this->checkSession()) {
            $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
            $sqllayer->updateSession(session_id());
            $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
            $message = htmlentities(strip_tags($_POST['message']));
            if (($message) != "\n") {
                if (preg_match($reg_exUrl, $message, $url)) {
                    $message = preg_replace($reg_exUrl, '<a href="' . $url[0] . '" target="_blank">' . $url[0] . '</a>', $message);
                }
                // fwrite(fopen('chat.txt', 'a'), "<span>" . $nickname . "</span>" . $message = str_replace("\n", " ", $message) . "\n");;
                $sqllayer->addMessage($this->user, $message);

                return $this->returnChatdata();
            }


        } else {
            return ["AUTH" => "LOGIN", "MESSAGE" => "LOGOUT DU AU TIME-OUT", "LMSG" => [], "LUSERS" =>[]];
        }
    }


    /**
     * crée le login -> renvoie en loggé au panneau de Chat
     * @return $response array
     */
    public function createLogin()
    {
        $wlogin = $this->post['login'];
        $wpassword = $this->post['password'];
        $reponse = [];
        $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
        $user = $sqllayer->createUser($wlogin, $wpassword);
        if ($user == null) {
            $reponse = ["AUTH" => "LOGIN", "MESSAGE" => "COMPTE CREE, LOGGING POSSIBLE", "LMSG" => [], "LUSERS" =>[]];
            return $reponse;
        } else {
            session_start();
            $sid = session_id();
            $this->user = $user;
            $this->session = $sqllayer->CreateOrUpdateSession($sid, $user);
            return $this->returnChatdata();
        }
    }

    /**
     * Test la session. Si OK envoie le update de chat
     * @return $response array
     */
    public function update(){
        if ($this->checkSession()) {
            $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
            session_start();
            $sid = session_id();
            $sqllayer->updateSession(session_id());
            return $this->returnChatdata();
            }
        else
            return  $this->logout();

    }

    /**
     * renvoie les contenus du panneau Chat et le statut "CHAT"
     * @return $response array
     */
    private function returnChatdata()
    {
        $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
        $lmsg = $sqllayer->getMessages();
        $lusers = $sqllayer->getLoggeduser();


        $reponse = ["AUTH" => "CHAT","MESSAGE" => "CHAT CONTENT","LMSG" => $lmsg, "LUSERS" => $lusers];
        return $reponse;
    }


    /**
     * Vérifie la session et renvoie trois cas
     *  session inexistante -> panneau de login
     *  session en time out -> panneau de login
     *  login ok -> traite message envoyé et rafraichit le panneau de chat
     * @return $response array
     */
    public function checkSession()
    {
        $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
        session_start();
        $sid = session_id();
        /**
         * @var $session IADSession
         */
        $session = $sqllayer->getSession($sid);
        if ($session != false) {
            $now = new \DateTime();

            $dateSes = $session->getLastupdate();
            $datelimite = $dateSes->add(date_interval_create_from_date_string('600 seconds'));

            if ($now < $datelimite) {
                $this->user = $session->getUser();
                $this->session = $session;
                return true;
            };
        }
        return false;
    }

    /**
     * Détruit la session
     * @return $response array
     */
    public function logout()
    {
        $sqllayer = new \IADchat\src\Model\Mysqlayer\IADMysqlLayer();
        session_start();
        $id = session_id();
        $sqllayer->removeSession($id);
        session_destroy();
        $this->session = null;
        $this->user = null;
        $reponse = ["AUTH" => "LOGIN", "MESSAGE"=>"LOGOUT OK", "LMSG" => [], "LUSERS" =>[]];
        return $reponse;
    }
}